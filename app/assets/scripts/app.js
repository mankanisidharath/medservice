import $ from 'jquery';
import '../styles/styles.css';
import 'lazysizes';
import MobileMenu from './modules/MobileMenu';
import RevealOnScroll from './modules/RevealOnScroll';
import SmoothScroll from './modules/SmoothScroll';
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";

// Handles Mobile Menu/Header
let mobileMenu = new MobileMenu();

// Handles Reveal On Scroll
new RevealOnScroll($('#our-beginning'));
new RevealOnScroll($('#departments'));
new RevealOnScroll($('#counters'));
new RevealOnScroll($('#testimonials'));

// Adding smooth scroll functionality to our header links
new SmoothScroll();

//Adding Active Links status functionality to our header links
new ActiveLinks();

//handling event for appointment modal
new Modal(); 

if(module.hot) {
	module.hot.accept();
}

console.log('Hello world from MedService');