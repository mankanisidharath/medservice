class MobileMenu{
	constructor(){
		//1. store all the needed dom elements in object
		this.menuIcon = document.querySelector(".mobile-header-icon");
		this.mobileHeader = document.querySelector("#mobile-header");

		//2. can create any class/objects variables which are needed
		this.x = 10; // just an example (x var is of no use)

		//3. call a function which registers all the required events
		this.events();
	}

	events(){
		this.menuIcon.addEventListener('click', () => this.toggleMenu());
	}

	toggleMenu(){
		this.mobileHeader.classList.toggle("mobile-menu-active");
		this.menuIcon.classList.toggle("mobile-header-icon-close");
	}
}

export default MobileMenu;